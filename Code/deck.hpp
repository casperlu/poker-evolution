#ifndef deck_hpp
#define deck_hpp


//Included dependencies
#include "card.hpp"
#include "hand.hpp"
#include <vector>
#include <exception>  // or #include <stdexcept>

//Forward decklare Hand
class Hand;

class Deck{
private:
	std::vector<Card> deck;

public:
	Deck(); //Constructor
	~Deck(); //Deconstructor
	

	void createStandardDeck(); //52 playing cards. Initiated in order: CLUBS: ace, 2, 3,..King, ; Diamonds: ace-king, Hearts: ace-king ; Spades: ace-king
	void shuffle(unsigned int seed);
	
	Card drawCardFromTop(); //Draws from the top, i.e. deck.end();
	void addCardToBottom(Card &c);
	void print();

	Hand drawHand(int numCards);
};


//Generic exception. Copy pasta from internet
struct QuitNow: public std::exception {
	
	QuitNow() { }
	virtual ~QuitNow() throw() { }
	virtual const char* what() throw() {
		return "QuitNow: request normal termination of program.\n"
		"(You should not see this message. Please report it if you do.)";
	}
};

#endif /* deck_hpp */
