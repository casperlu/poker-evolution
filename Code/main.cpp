#include "pokerHand.hpp"
#include "player.hpp"
#include <iostream>
#include <limits>

using namespace std;

int main (int argc, char* argv[]) {

	//int seed = atoi(argv[1]);
	
	int numSimulations = 1e6;
	vector<int> samplingTimes = {0,1,2,3,4,5,10,20,50,100,1000,(int)1e6};	
	vector<vector<int> > typeCounts; //Counts for each type at each sampling time
	for(unsigned int i=0; i<samplingTimes.size(); i++) {
		vector<int> typeCountsTimePoint(10,0); //There are 10 different types, set all counts to zero
		typeCounts.push_back(typeCountsTimePoint);
	}

	for(int i=1; i<=numSimulations; i++) {
		if(i%(numSimulations/100)==0) { cout << "Simulation " << i << " of " << numSimulations << ".\n"; }

		//Set up random number generator
		srand(i);

		//Create and shuffle deck
		Deck d;
		d.createStandardDeck();
		d.shuffle(rand()%1000000); //Shuffle using some random seed

		Hand h = d.drawHand(5);
		PokerHand ph = PokerHand(h);
//		cout << "Initial poker hand is:\n";
//		ph.print();
//		cout << "\n";
		Player p;
		p.loadPokerHand(ph);

		bool maximumFound = false;
		int samplingCounter = 0;
		for(int generation = 0; generation < numeric_limits<int>::max(); generation++) { //Loop over generations

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Old method; three cards were drawn and the best possible swap with any of the cards in the pokerhand was chosen. //
//														    //
//			if(generation > 0) { //Don't update initial hand					    //
//				Hand threeCards = d.drawHand(3);						    //
//				p.updatePokerHand(threeCards);							    //
//				threeCards.putBackInDeck(d);							    //
//			}											    //
//														    //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			//New method: single card is picked from deck and compared to a card chosen at random in pokerhand.
			//If new card represents a beneficial (or neutral) mutation, the new card replaces the old card.
			if(generation > 0) { //Don't update initial hand
				Card singleCard = d.drawCardFromTop();
				int pokerHandIndex = rand()%5; //Random number between 0 and 4
				p.updatePokerHandMutation(singleCard, pokerHandIndex); //If succesful replacement, old card is returned as singleCard
				d.addCardToBottom(singleCard);

			}			
			Type t = p.getType();

			switch(t) {

				case Type::FOUR_OF_A_KIND : maximumFound = true; break;
				case Type::ROYAL_FLUSH : maximumFound = true; break;
				default : maximumFound = false; break;

			}
		
			if(samplingTimes[samplingCounter] == generation) { //Sampling time occurred

				int typeInt = static_cast<int>(t); //E.g. 1 = high card, 2 = pair, etc.
				typeCounts[samplingCounter][typeInt-1]++; //Increment said type
				samplingCounter++;

				if(samplingCounter == (int) samplingTimes.size()) { break; } //No more sampling points to investigate
			}	
	
			if(maximumFound == true) { //Local maximum found. No way to improve. Exit simulation here.

				//If some sampling times have not been reached, fill them in based on local maximum
				for(; samplingCounter < (int) samplingTimes.size(); samplingCounter++) {
					int typeInt = static_cast<int>(t); //E.g. 1 = high card, 2 = pair, etc.
					typeCounts[samplingCounter][typeInt-1]++; //Increment said type
				}

				break; //Exit current simulation
			}	
		}

	}

	cout << "\n"; //Leave space before printing stats

	vector<string> typeNames = { "High card: \t\t","One pair: \t\t","Two pairs: \t\t","Three of a kind: \t","Straight: \t\t","Flush: \t\t\t","Full house: \t\t","Four of a kind: \t","Straight flush: \t","Royal flush: \t\t"};
	for(unsigned int i=0; i<samplingTimes.size(); i++) {

		cout << "Outcomes (in percent) for sampling time " << samplingTimes[i] << ":\n";

		for(unsigned int j=0; j<typeCounts[i].size(); j++) {

			cout << typeNames[j] << (typeCounts[i][j]/((double)numSimulations)) << "\n";
		}
		cout << "\n";
	}
		
			
	return 0;
}

