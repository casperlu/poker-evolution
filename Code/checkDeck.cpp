#include "pokerHand.hpp"
#include <iostream>

using namespace std;

int main (int argc, char* argv[]) {

	for(int i=1; i<=5; i++) { //Five decks

		Deck d;
		d.createStandardDeck();
	
		//cout << "Printing standard deck:\n";
		//d.print();
		//cout << "\n";

		d.shuffle(i);
		//cout << "Printing shuffled deck:\n";
		//d.print();
		//cout << "\n";

		for(int j=0; j<10; j++) { //Ten hands
			cout << "Deck " << i << " hand " << j << ":\n";
//			cout << "Draw a poker hand of 5 cards:\n";
			Hand h = d.drawHand(5);
			PokerHand ph = PokerHand(h);
			ph.print();
			cout << "\n";
		}
	}



	//Create all types
	//Royal flush
	Hand h;
	Card c11 = Card(1,10);
	Card c12 = Card(1,11);
	Card c13 = Card(1,12);
	Card c14 = Card(1,13);
	Card c15 = Card(1,1);
	h.addCard(c11);	
	h.addCard(c12);	
	h.addCard(c13);	
	h.addCard(c14);	
	h.addCard(c15);	
	PokerHand ph = PokerHand(h);
	ph.print();
	cout << "\n";

	//Straight flush
	Hand h2;
	Card c21 = Card(1,10);
	Card c22 = Card(1,11);
	Card c23 = Card(1,12);
	Card c24 = Card(1,13);
	Card c25 = Card(1,9);
	h2.addCard(c21);	
	h2.addCard(c22);	
	h2.addCard(c23);	
	h2.addCard(c24);	
	h2.addCard(c25);	
	PokerHand ph2 = PokerHand(h2);
	ph2.print();
	cout << "\n";

	//Four of a kind
	Hand h3;
	Card c31 = Card(1,6);
	Card c32 = Card(2,6);
	Card c33 = Card(3,6);
	Card c34 = Card(4,6);
	Card c35 = Card(1,9);
	h3.addCard(c31);	
	h3.addCard(c32);	
	h3.addCard(c33);	
	h3.addCard(c34);	
	h3.addCard(c35);	
	PokerHand ph3 = PokerHand(h3);
	ph3.print();
	cout << "\n";

	//Full house
	Hand h4;
	Card c41 = Card(1,6);
	Card c42 = Card(2,6);
	Card c43 = Card(3,6);
	Card c44 = Card(1,12);
	Card c45 = Card(2,12);
	h4.addCard(c41);	
	h4.addCard(c42);	
	h4.addCard(c43);	
	h4.addCard(c44);	
	h4.addCard(c45);	
	PokerHand ph4 = PokerHand(h4);
	ph4.print();
	cout << "\n";

	//Flush
	Hand h5;
	Card c51 = Card(3,2);
	Card c52 = Card(3,4);
	Card c53 = Card(3,6);
	Card c54 = Card(3,12);
	Card c55 = Card(3,7);
	h5.addCard(c51);	
	h5.addCard(c52);	
	h5.addCard(c53);	
	h5.addCard(c54);	
	h5.addCard(c55);	
	PokerHand ph5 = PokerHand(h5);
	ph5.print();
	cout << "\n";

	//Straight
	Hand h6;
	Card c61 = Card(3,10);
	Card c62 = Card(2,11);
	Card c63 = Card(1,12);
	Card c64 = Card(4,13);
	Card c65 = Card(3,1);
	h6.addCard(c61);	
	h6.addCard(c62);	
	h6.addCard(c63);	
	h6.addCard(c64);	
	h6.addCard(c65);	
	PokerHand ph6 = PokerHand(h6);
	ph6.print();
	cout << "\n";

	//Three of a kind
	Hand h7;
	Card c71 = Card(2,2);
	Card c72 = Card(3,2);
	Card c73 = Card(4,2);
	Card c74 = Card(1,12);
	Card c75 = Card(2,7);
	h7.addCard(c71);	
	h7.addCard(c72);	
	h7.addCard(c73);	
	h7.addCard(c74);	
	h7.addCard(c75);	
	PokerHand ph7 = PokerHand(h7);
	ph7.print();
	cout << "\n";

	//Two pairs
	Hand h8;
	Card c81 = Card(1,2);
	Card c82 = Card(2,2);
	Card c83 = Card(2,7);
	Card c84 = Card(3,7);
	Card c85 = Card(4,5);
	h8.addCard(c81);	
	h8.addCard(c82);	
	h8.addCard(c83);	
	h8.addCard(c84);	
	h8.addCard(c85);	
	PokerHand ph8 = PokerHand(h8);
	ph8.print();
	cout << "\n";

	//One pair
	Hand h9;
	Card c91 = Card(2,5);
	Card c92 = Card(2,4);
	Card c93 = Card(3,12);
	Card c94 = Card(4,5);
	Card c95 = Card(1,7);
	h9.addCard(c91);	
	h9.addCard(c92);	
	h9.addCard(c93);	
	h9.addCard(c94);	
	h9.addCard(c95);	
	PokerHand ph9 = PokerHand(h9);
	ph9.print();
	cout << "\n";

	//High card
	Hand h10;
	Card c101 = Card(3,11);
	Card c102 = Card(2,4);
	Card c103 = Card(3,6);
	Card c104 = Card(4,2);
	Card c105 = Card(3,7);
	h10.addCard(c101);	
	h10.addCard(c102);	
	h10.addCard(c103);	
	h10.addCard(c104);	
	h10.addCard(c105);	
	PokerHand ph10 = PokerHand(h10);
	ph10.print();
	cout << "\n";




	return 0;
}

