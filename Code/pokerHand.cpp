#include "pokerHand.hpp"
#include <algorithm>

using namespace std;

PokerHand::PokerHand() { }

PokerHand::PokerHand(Hand &h) {

	if(h.getNumCards() == 5) { //Note, this value has been hardcoded in some classes, e.g. Player. If changed from 5, all these instances must be updated too.
		cards = h.getCards();
		updateType();
	} else {
		cout << "A poker hand must have exactly 5 cards. This hand has " << h.getNumCards() << ". Exiting.\n";
		throw QuitNow();
	}
}

//Update value of type by working through the grid systematically, starting with the royal flush and progressing towards a high card
void PokerHand::updateType() {

	//Start by cheching if all cards are of the same kind, i.e. a flush - this is the fastest way to rule out royal flush (and straight flush for that matter)
	bool flush = checkForFlush();

	if(flush == true) { //Check types involving flush

		updateRegion1();

	} else { //Check types not involving flush

		updateRegion2();
	}

}

Type PokerHand::getType() { return type; }


bool PokerHand::checkForFlush() {

	bool flush = true;
	Suit s0 = cards[0].getSuit();

	for(unsigned int i=1; i<5; i++) {

		Suit s = cards[i].getSuit();
	
		if(s != s0) { flush = false; break; }	
	}

	return flush;
}

//Case: Flush has been diagnosed. Check for anything higher (i.e. straight flush and royal flush since
//four of a kind and full house impossible under flush condition)
void PokerHand::updateRegion1() {

	//Check for straight
	bool straight = checkForStraight();

	if(straight == false) { 
		type = Type::FLUSH;

	} else {
		
		bool royalFlush = checkForRoyal();

		if(royalFlush == true) {
			type = Type::ROYAL_FLUSH;
		} else{
			type = Type::STRAIGHT_FLUSH;
		}
	}
}

//Update given that flush is not true. This leaves four of a kind, full house, straight, three of a kind, two pairs, one pair and high card
void PokerHand::updateRegion2() {

	//First we check for n of a kind
	int n = findNOfAKind();


	if(n==4) { //Four of a kind

		type = Type::FOUR_OF_A_KIND;

	} else if (n==3) {

		updateRegion3(); //Full house or three of a kind

	} else if (n==2) {

		updateRegion4(); //Two pairs or one pair

	} else {

		updateRegion5(); //Straight or high card
	}

}

bool PokerHand::checkForStraight() {


	bool straight = true;
	int v0 = static_cast<int>(cards[0].getValue());
	int v1 = static_cast<int>(cards[1].getValue());
	int v2 = static_cast<int>(cards[2].getValue());
	int v3 = static_cast<int>(cards[3].getValue());
	int v4 = static_cast<int>(cards[4].getValue());

	vector<int> v {v0,v1,v2,v3,v4};

	sort(v.begin(),v.end());

	//Check for straight
	for(unsigned int i=1; i<5; i++) {

		if(v[i] != (v[i-1] +1)) { straight = false; break; }

	}

	
	//Ace has value of 1, but can also be 14, so check for straight this way
	if(v[0]==1 && v[1]==10 && v[2]==11 && v[3]==12 && v[4]==13) { straight = true; } 

	return straight;
}

//Given that flush and straight have been diagnosed, check for "royal" i.e. 10,Jack,Queen,King,Ace
bool PokerHand::checkForRoyal() {

	bool royal = false;
	int v0 = static_cast<int>(cards[0].getValue());
	int v1 = static_cast<int>(cards[1].getValue());
	int v2 = static_cast<int>(cards[2].getValue());
	int v3 = static_cast<int>(cards[3].getValue());
	int v4 = static_cast<int>(cards[4].getValue());

	vector<int> v {v0,v1,v2,v3,v4};

	sort(v.begin(),v.end());

	//Ace has value of 1, but can also be 14, so check for straight this way
	if(v[0]==1 && v[1]==10 && v[2]==11 && v[3]==12 && v[4]==13) { royal = true; } 

	return royal;
}


int PokerHand::findNOfAKind() {

	int v0 = static_cast<int>(cards[0].getValue());
	int v1 = static_cast<int>(cards[1].getValue());
	int v2 = static_cast<int>(cards[2].getValue());
	int v3 = static_cast<int>(cards[3].getValue());
	int v4 = static_cast<int>(cards[4].getValue());

	vector<int> v {v0,v1,v2,v3,v4};
	vector<int> n {1,1,1,1,1};
	int maxN = 1;


	for(int i=0; i<5; i++) {

		for(int j=0; j<5; j++) {

			if(i!=j) {

				if(v[i]==v[j]) { n[i]++; }
			}
		}

		if(n[i] > maxN) { maxN = n[i]; }
	}

	return maxN;
}

//Check for full house or three of a kind
void PokerHand::updateRegion3() {

	bool pair = checkIfPair(); //Implies full house

	if(pair == true) {

		type = Type::FULL_HOUSE;

	} else {

		type = Type::THREE_OF_A_KIND;
	}

}

//Check for two pairs or a single pair
void PokerHand::updateRegion4() {

	bool twoPairs = checkIfTwoPairs();

	if(twoPairs == true) {

		type = Type::TWO_PAIRS;

	} else {

		type = Type::ONE_PAIR;
	}

}

//Check for straight or high card
void PokerHand::updateRegion5() {

	bool straight = checkForStraight();

	if(straight == true) {

		type = Type::STRAIGHT;

	} else {

		type = Type::HIGH_CARD;
	}
}

//Check if there is a pair
bool PokerHand::checkIfPair() {

	int v0 = static_cast<int>(cards[0].getValue());
	int v1 = static_cast<int>(cards[1].getValue());
	int v2 = static_cast<int>(cards[2].getValue());
	int v3 = static_cast<int>(cards[3].getValue());
	int v4 = static_cast<int>(cards[4].getValue());

	vector<int> v {v0,v1,v2,v3,v4};
	vector<int> n {1,1,1,1,1};

	for(int i=0; i<5; i++) {

		for(int j=0; j<5; j++) {

			if(i!=j) {

				if(v[i]==v[j]) { n[i]++; }
			}
		}

		if(n[i] == 2) { 
	
			//Pair found!
			return true;
		 }
	}

	return false;
}

//Check if there are two pairs
bool PokerHand::checkIfTwoPairs() {

	int v0 = static_cast<int>(cards[0].getValue());
	int v1 = static_cast<int>(cards[1].getValue());
	int v2 = static_cast<int>(cards[2].getValue());
	int v3 = static_cast<int>(cards[3].getValue());
	int v4 = static_cast<int>(cards[4].getValue());

	vector<int> v {v0,v1,v2,v3,v4};
	vector<int> n {1,1,1,1,1};
	int numTimesAPairIsFound = 0; //Not equal to number of actual pairs!

	for(int i=0; i<5; i++) {

		for(int j=0; j<5; j++) {

			if(i!=j) {

				if(v[i]==v[j]) { n[i]++; }
			}
		}

		if(n[i] == 2) { 
	
			//Pair found!
			numTimesAPairIsFound++;

			//Two pairs means that for  e.g. {4,5,8,4,5} --> n={2,2,1,2,2} so pairsFound == 4 in this case
			if(numTimesAPairIsFound == 4) {
				return true;
			}
		 }
	}

	return false;
}


void PokerHand::print() {

	//Print from super
	Hand::print();

	//Print type
	//Would be nice to check if initialised?
	switch(type) {
	
		case Type::HIGH_CARD : cout << "Type is high card.\n"; break;
		case Type::ONE_PAIR : cout << "Type is one pair.\n"; break;
		case Type::TWO_PAIRS : cout << "Type is two pairs.\n"; break;
		case Type::THREE_OF_A_KIND : cout << "Type is three of a kind.\n"; break;
		case Type::STRAIGHT : cout << "Type is straight.\n"; break;
		case Type::FLUSH : cout << "Type is flush.\n"; break;
		case Type::FULL_HOUSE : cout << "Type is full house.\n"; break;
		case Type::FOUR_OF_A_KIND : cout << "Type is four of a kind.\n"; break;
		case Type::STRAIGHT_FLUSH : cout << "Type is straight flush.\n"; break;
		case Type::ROYAL_FLUSH : cout << "Type is royal flush.\n"; break;

	}	

}
