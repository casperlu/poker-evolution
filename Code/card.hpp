#ifndef card_hpp
#define card_hpp


//Included dependencies
#include "suit.hpp"
#include "cardValue.hpp"


class Card{
private:
	Suit suit;	
	CardValue value;

public:
	Card(); //Constructor
	Card(Suit &s, CardValue &v);
	Card(int s, int v);
	~Card(); //Deconstructor

	
	Suit getSuit();
	CardValue getValue();
	void print();
};


#endif /* card_hpp */
