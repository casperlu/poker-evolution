#ifndef pokerHand_hpp
#define pokerHand_hpp


//Included dependencies
#include "hand.hpp"
#include "deck.hpp"
#include <vector>
#include <iostream>

//Forward declared enum. Defined below class.
enum struct Type;

class PokerHand : public Hand {
private:
	Type type;

public:

	//Constructor
	PokerHand(); //Constructor does nothing
	PokerHand(Hand &h); //Create object from generic hand
	
	void updateType();
	Type getType();

	bool checkForFlush();
	void updateRegion1(); //Flush, straight flush and royal flush
	void updateRegion2(); // Four of a kind, full house, straight, three of a kind, two pairs, one pair, high card
	bool checkForStraight();
	bool checkForRoyal();
	int findNOfAKind(); //Finds the largest number of same value cards
	void updateRegion3(); //Full house  or three of a kind
	void updateRegion4(); //Two pairs or one pair
	void updateRegion5(); //Straight or high card
	bool checkIfPair(); //Check if there is a pair (doesn't differentiate between one and two pairs!)
	bool checkIfTwoPairs(); //Check if there are two pairs

	void print();

};

enum struct Type {

	HIGH_CARD = 1,
	ONE_PAIR = 2,
	TWO_PAIRS = 3,
	THREE_OF_A_KIND = 4,
	STRAIGHT = 5,
	FLUSH = 6,
	FULL_HOUSE = 7,
	FOUR_OF_A_KIND = 8,
	STRAIGHT_FLUSH = 9,
	ROYAL_FLUSH = 10

};


#endif /* pokerHand_hpp */
