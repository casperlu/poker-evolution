#include "deck.hpp"
#include <random>
#include <algorithm>

using namespace std;

//Class related methods + constructors
Deck::Deck() { } //Constructor does nothing - deck not initiated

Deck::~Deck() { } //Deconstructor


void Deck::createStandardDeck() {

	deck.clear(); //Remove any existing cards

	for(int s=1; s<=4; s++) { //Suit index
	
		for(int v=1; v<=13; v++) { //Value index

			Card c = Card(s,v);
			deck.push_back(c);
		}
	}

}

void Deck::shuffle(unsigned int seed) {
	
	//Explicitly specify this is the std::shuffle method to avoid confusion
	std::shuffle(deck.begin(), deck.end(), default_random_engine(seed));
}


Card Deck::drawCardFromTop() {
	
	Card draw = deck[deck.size()-1]; //Grab the last element
	deck.pop_back();

	return draw;
}

void Deck::addCardToBottom(Card &c) {

	deck.insert(deck.begin(), c);
}


void Deck::print() { 

	for(unsigned int i=0; i<deck.size(); i++) {
		
		deck[i].print();
	}
}


Hand Deck::drawHand(int numCards) {

	Hand h;

	if(numCards <= (int) deck.size()) {

		for(int i=0; i<numCards; i++) {

			Card c = drawCardFromTop();
			h.addCard(c);
		}

	} else { 
		cout << "Cannot draw " << numCards << " cards as deck only contains " << deck.size() << " cards.\n";
		throw QuitNow();
	}

	return h;
}


