#include "card.hpp"
#include <iostream>

using namespace std;

//Class related methods + constructors
Card::Card() { } //Constructor does nothing - card not initiated
Card::Card(Suit &s, CardValue &v) : suit(s), value(v) {

}
Card::Card(int s, int v) : suit(static_cast<Suit>(s)), value(static_cast<CardValue>(v)) { }
Card::~Card() { } //Deconstructor

Suit Card::getSuit() { return suit; }
CardValue Card::getValue() { return value; }

void Card::print() {

	switch(value) {

		case CardValue::ACE : cout << "Ace of "; break;
		case CardValue::TWO : cout << "Two of "; break;
		case CardValue::THREE : cout << "Three of "; break;
		case CardValue::FOUR : cout << "Four of "; break;
		case CardValue::FIVE : cout << "Five of "; break;
		case CardValue::SIX : cout << "Six of "; break;
		case CardValue::SEVEN : cout << "Seven of "; break;
		case CardValue::EIGHT : cout << "Eight of "; break;
		case CardValue::NINE : cout << "Nine of "; break;
		case CardValue::TEN : cout << "Ten of "; break;
		case CardValue::JACK : cout << "Jack of "; break;
		case CardValue::QUEEN : cout << "Queen of "; break;
		case CardValue::KING : cout << "King of "; break;
	}	

	switch(suit) {

		case Suit::CLUBS : cout << "clubs.\n"; break;
		case Suit::DIAMONDS : cout << "diamonds.\n"; break;
		case Suit::HEARTS : cout << "hearts.\n"; break;
		case Suit::SPADES : cout << "spades.\n"; break;
	}
}

