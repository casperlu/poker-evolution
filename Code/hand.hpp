#ifndef hand_hpp
#define hand_hpp


//Included dependencies
#include "deck.hpp"
#include <vector>
#include <exception>  // or #include <stdexcept>
#include <iostream>

//Forward declare deck
class Deck;


class Hand{
protected:
	std::vector<Card> cards;

public:
	Hand(); //Constructor
	~Hand(); //Deconstructor

	void addCard(Card &c); //NOTE: a hand has no top or bottom. There is no order, so this method just appends the card.
	std::vector<Card> getCards();
	std::vector<int> getCardValues();
	std::vector<int> getSuitCounts(); //Returns e.g. {3,0,1,1} = 3 clubs, 0 diamonds, 1 heart, 1 spade
	Card getCard(int index);
	void setCard(int index, Card &c);
	int getNumCards();
	void print();
	void putBackInDeck(Deck &d);
};


#endif /* hand_hpp */
