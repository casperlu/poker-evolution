#ifndef player_hpp
#define player_hpp

#include "pokerHand.hpp"


class Player{
private:
	PokerHand pokerHand;

public:
	Player(); //Constructor
	~Player(); //Deconstructor

	void loadPokerHand(PokerHand &ph);	
	void updatePokerHand(Hand &h);
	void updatePokerHandMutation(Card &newCard, int pokerHandIndex);
	bool comparePokerHands(PokerHand &ph1, PokerHand &ph2); //Returns true if 1 is better than 2
	Type getType();
	void print();


	//Comparing hands of the same time
	bool compareHighCard(PokerHand &ph1, PokerHand &ph2);
	bool compareOnePair(PokerHand &ph1, PokerHand &ph2);
	bool compareTwoPairs(PokerHand &ph1, PokerHand &ph2);
	bool compareThreeOfAKind(PokerHand &ph1, PokerHand &ph2);
	bool compareStraight(PokerHand &ph1, PokerHand &ph2);
	bool compareFlush(PokerHand &ph1, PokerHand &ph2);
	bool compareFullHouse(PokerHand &ph1, PokerHand &ph2);
	bool compareFourOfAKind(PokerHand &ph1, PokerHand &ph2);
	bool compareStraightFlush(PokerHand &ph1, PokerHand &ph2);
};


#endif /* player_hpp */
