#include "player.hpp"
#include <algorithm>

using namespace std;

//Class related methods + constructors
Player::Player() { } //Constructor does nothing - hand not initiated

Player::~Player() { } //Deconstructor

void Player::loadPokerHand(PokerHand &ph) { pokerHand = ph; }

void Player::updatePokerHand(Hand &h) {

	//For each card in the hand, try updating pokerhand
	//If pokerhand improved, store the new pokerhand
	//Finally, compare all the new pokerhands and accept the best
	
	int bestSwapHandIndex = -1; //Unsassigned
	int bestSwapPokerHandIndex = -1;
	PokerHand bestPH = pokerHand; //Original is best to begin with
	PokerHand newPH = pokerHand;

	for(int i=0; i<h.getNumCards(); i++) { //Loop over cards in hand

		Card cardToSwap = h.getCard(i);

		for(int j=0; j<5; j++) { //Loop over cards in pokerhand
	
			newPH = pokerHand;
			newPH.setCard(j,cardToSwap);
			newPH.updateType();
			bool improvementFound = comparePokerHands(newPH, bestPH);

			if(improvementFound == true) {

				bestPH = newPH;
				bestSwapHandIndex = i;
				bestSwapPokerHandIndex = j;
			}
		}
	}




	if(bestSwapHandIndex != -1) { //Better hand found, so update it

		Card newCard = h.getCard(bestSwapHandIndex);
		Card oldCard = pokerHand.getCard(bestSwapPokerHandIndex);
		pokerHand.setCard(bestSwapPokerHandIndex, newCard);
		pokerHand.updateType();
		h.setCard(bestSwapHandIndex, oldCard);

		//cout << "Poker hand was updated. Hand is now:\n";
		//pokerHand.print();
		//cout << "\n";
	}
}


void Player::updatePokerHandMutation(Card &c, int pokerHandIndex) {

	//Create a new poker hand by replacing the card at pokerHandIndex with the new card (mutation) c
	PokerHand oldPH = pokerHand; //Original hand
	PokerHand newPH = pokerHand;
	newPH.setCard(pokerHandIndex, c);
	newPH.updateType();

	//Check if new pokerhand is better than the old
	bool improvementFound = comparePokerHands(newPH, oldPH);

	if(improvementFound == true) {

		Card newCard = c;
		Card oldCard = pokerHand.getCard(pokerHandIndex);
		pokerHand.setCard(pokerHandIndex, newCard); //Not passed as pointer, so can change newCard afterwards without changing pokerhand
		pokerHand.updateType();
		c = oldCard;
	}

}

bool Player::comparePokerHands(PokerHand &ph1, PokerHand &ph2) {

	int type1 = static_cast<int>(ph1.getType());
	int type2 = static_cast<int>(ph2.getType());

	//cout << "Type1: " << type1 << " type 2: " << type2 << "\n";

	if(type1 > type2) {

		return true;

	} else if(type2 > type1) {

		return false;
	
	} else {

		//Compare hands of same type - more complicated
		switch(ph1.getType()) {

			case Type::HIGH_CARD : return compareHighCard(ph1, ph2); break;
			case Type::ONE_PAIR : return compareOnePair(ph1, ph2); break;
			case Type::TWO_PAIRS : return compareTwoPairs(ph1, ph2); break;
			case Type::THREE_OF_A_KIND : return compareThreeOfAKind(ph1, ph2); break;
			case Type::STRAIGHT : return compareStraight(ph1, ph2); break;
			case Type::FLUSH : return compareFlush(ph1, ph2); break;
			case Type::FULL_HOUSE : return compareFullHouse(ph1, ph2); break;
			case Type::FOUR_OF_A_KIND : return compareFourOfAKind(ph1, ph2); break;
			case Type::STRAIGHT_FLUSH : return compareStraightFlush(ph1, ph2); break;
			case Type::ROYAL_FLUSH : return true; //If both hands are royal flush, they have same fitness, so can swap between states with no penalty

		}
		return false;
	} 

}

Type Player::getType() { return pokerHand.getType(); }
void Player::print() { pokerHand.print(); }

//Returns true if ph1 is the better hand (if tie, return true as well)
bool Player::compareHighCard(PokerHand &ph1, PokerHand &ph2) {

	vector<int> cards1 = ph1.getCardValues(); //By definition sets ace to 14
	vector<int> cards2 = ph2.getCardValues();

	//Sort in descending order
	sort(cards1.begin(), cards1.end(), greater<int>());
	sort(cards2.begin(), cards2.end(), greater<int>());

	//Loop over values in descending order
	//If cards1[i] > cards2[i] then return true, otherwise false
	for(unsigned int i=0; i<cards1.size(); i++) {

		if(cards1[i] > cards2[i]) {

			return true;

		} else if(cards2[i] > cards1[i]) {

			return false;
		} 
	}

	//If tie, return true	
	return true;
}

bool Player::compareOnePair(PokerHand &ph1, PokerHand &ph2) {

	vector<int> cards1 = ph1.getCardValues(); //By definition sets ace to 14
	vector<int> cards2 = ph2.getCardValues();

	int pair1 = -1;
	int pair2 = -1;

	for(int i=0; i<5; i++) {

		for(int j=0; j<5; j++) {

			if(i!=j) {
		
				if(cards1[i] == cards1[j]) { pair1 = cards1[i]; }
				if(cards2[i] == cards2[j]) { pair2 = cards2[i]; }
			}
		}
	}
	
	if(pair1 > pair2) { 
		
		return true;

	} else if (pair2 > pair1) {

		return false;
	
	} else { //Pairs are of same value, so tie breaker goes to subsequent highest cards

		//First remove the pairs
		for(int i=4; i>=0; i--) { //Go through in reverse to ensure no issue when removing

			if(cards1[i] == pair1) { //Remove this value from cards
				cards1.erase(cards1.begin() + i);
			}
			if(cards2[i] == pair2) { //Remove value
				cards2.erase(cards2.begin() + i);
			}
		}

		//Sort remaining three cards in descending order
		sort(cards1.begin(), cards1.end(), greater<int>());
		sort(cards2.begin(), cards2.end(), greater<int>());

		for(int i=0; i<3; i++) { //Three cards to chose from

			if(cards1[i] > cards2[i]) {

				return true;

			} else if (cards2[i] > cards1[i]) {

				return false;
			}
		}

		//If a tie, return true
		return true;
	}
}


bool Player::compareTwoPairs(PokerHand &ph1, PokerHand &ph2) {

	vector<int> cards1 = ph1.getCardValues(); //By definition sets ace to 14
	vector<int> cards2 = ph2.getCardValues();

	int pair1a = -1;
	int pair2a = -1;	
	bool pair1aFound = false;
	bool pair2aFound = false;

	//Find a pair for both hands (doesn't matter which pair)
	for(int i=0; i<5; i++) {
	
		for(int j=0; j<5; j++) {

			if(i!=j) {

				if(cards1[i] == cards1[j]) { pair1a = cards1[i]; pair1aFound = true; }
				if(cards2[i] == cards2[j]) { pair2a = cards2[i]; pair2aFound = true; }
			}
		}

		if(pair1aFound == true && pair2aFound == true) { break; }
	}

	//Find second pair
	int pair1b = -1; 
	int pair2b = -1;
	bool pair1bFound = false;
	bool pair2bFound = false;

	for(int i=0; i<5; i++) {

		for(int j=0; j<5; j++) {

			if(i!=j) {

				if(cards1[i] == cards1[j] && cards1[i] != pair1a) { 
					pair1b = cards1[i];
					pair1bFound = true;
				}

				if(cards2[i] == cards2[j] && cards2[i] != pair2a) { 
					pair2b = cards2[i];
					pair2bFound = true;
				}
			}
		}

		if(pair1bFound == true && pair2bFound == true) { break; }
	}

	//Find side card
	int sideCard1 = -1;
	int sideCard2 = -1;

	for(int i=0; i<5; i++) {

		if(cards1[i] != pair1a && cards1[i] != pair1b) { sideCard1 = cards1[i]; }
		if(cards2[i] != pair2a && cards2[i] != pair2b) { sideCard2 = cards2[i]; }
	}


	//Highest pair wins, then next highes, then highest side card	
	if(pair1b > pair1a) { int temp = pair1a; pair1a = pair1b; pair1b = temp; } //Order so that pair1a > pair1b
	if(pair2b > pair2b) { int temp = pair2a; pair2a = pair2b; pair2b = temp; }


	if(pair1a > pair2a) {
	
		return true;		

	} else if (pair2a > pair1a) {

		return false;		

	} else { //Same highest pair

		if(pair1b > pair2b) {

			return true;
		
		} else if (pair2b > pair1b) {

			return false;
	
		} else { //Both pairs the same

			if(sideCard1 > sideCard2) {
	
				return true;

			} else if (sideCard2 > sideCard1) {

				return false;

			} else{

				return true; //If tie, return true
			}
		}
	}
}

//Returns true if ph1 better hand than ph2. Also returns true if tie
bool Player::compareThreeOfAKind(PokerHand &ph1, PokerHand &ph2) {

	vector<int> cards1 = ph1.getCardValues(); //By definition sets ace to 14
	vector<int> cards2 = ph2.getCardValues();


	int threeOfAKind1 = -1;
	int threeOfAKind2 = -1;
	for(int i=0; i<5; i++) {

		int sameCount1 = 0; //Count the number of cards identical to card i
		int sameCount2 = 0;
		for(int j=0; j<5; j++) {

			if(i!=j) {
		
				if(cards1[i] == cards1[j]) { sameCount1++; }
				if(cards2[i] == cards2[j]) { sameCount2++; }		
			}
		}

		if(sameCount1 == 3) { threeOfAKind1 = cards1[i]; }
		if(sameCount2 == 3) { threeOfAKind2 = cards2[i]; }
		if(threeOfAKind1 != -1 && threeOfAKind2 != -1) { break; }
	}


	if(threeOfAKind1 > threeOfAKind2) { 

		return true; 

	} else {

		return false;
	} 
}

//Return true if ph1 is better than ph2
bool Player::compareStraight(PokerHand &ph1, PokerHand &ph2) {

	vector<int> cards1 = ph1.getCardValues(); //By definition sets ace to 14
	vector<int> cards2 = ph2.getCardValues();

	//Sort cards in descending order
	sort(cards1.begin(), cards1.end(), greater<int>());
	sort(cards2.begin(), cards2.end(), greater<int>());

	//If highest card is an ace, need to be more careful as ace can be both 1 and 14 in straight
	if(cards1[0] == 14 && cards2[0] ==14 ) {

		int highest1;
		if(cards1[1]==13) { highest1 = 14; }
		else { highest1 = cards1[1]; } //I.e. 5

		int highest2;
		if(cards2[1]==13) { highest2 = 14; }
		else {highest2 = cards2[1]; }

		//Case where both hands are highest possible straights
		//To avoid a possible infinite loop, only accept ph1 as better than ph2 IFF ph1 has more cards of one suit than ph2
		if(highest1 == 14 && highest2 == 14) {

			vector<int> numSuits1 = ph1.getSuitCounts();	
			vector<int> numSuits2 = ph2.getSuitCounts();
			int highestCount1 = 0;	
			int highestCount2 = 0;	

			for(unsigned int i=0; i<numSuits1.size(); i++) {
				if(numSuits1[i] > highestCount1) { highestCount1 = numSuits1[i]; }
				if(numSuits2[i] > highestCount2) { highestCount2 = numSuits2[i]; }
			}

			if(highestCount1 > highestCount2) { return true; }
			else { return false; }

		} 
		else if(highest1 >= highest2) { return true; }
		else { return false; }
		
	} else if(cards1[0] == 14) {

		int highest1;
		if(cards1[1]==13) { highest1 = 14; }
		else { highest1 = cards1[1]; }
		
		if(highest1 > cards2[0]) { return true; }
		else { return false; }

	} else if(cards2[0] == 14) {
	
		int highest2;
		if(cards2[1]==13) { highest2 = 14; }
		else { highest2 = cards2[1]; }

		if(cards1[0] > highest2) { return true; }
		else { return false; }

	} else {

		if(cards1[0] >= cards2[0]) {

			return true;

		} else {

			return false;
		}	
	}
}

//Return true if ph1 better hand than ph2
bool Player::compareFlush(PokerHand &ph1, PokerHand &ph2) {

	return compareHighCard(ph1, ph2); //Same rule for ties	
}

//Return true if ph1 better hand than ph2
bool Player::compareFullHouse(PokerHand &ph1, PokerHand &ph2) {

	return compareThreeOfAKind(ph1, ph2); //Same rule for ties
}


//Return true if ph1 better than ph2
bool Player::compareFourOfAKind(PokerHand &ph1, PokerHand &ph2) {

	vector<int> cards1 = ph1.getCardValues(); //By definition sets ace to 14
	vector<int> cards2 = ph2.getCardValues();


	int fourOfAKind1 = -1;
	int fourOfAKind2 = -1;
	for(int i=0; i<5; i++) {

		int sameCount1 = 0; //Count the number of cards identical to card i
		int sameCount2 = 0;
		for(int j=0; j<5; j++) {

			if(i!=j) {
		
				if(cards1[i] == cards1[j]) { sameCount1++; }
				if(cards2[i] == cards2[j]) { sameCount2++; }		
			}
		}

		if(sameCount1 == 4) { fourOfAKind1 = cards1[i]; }
		if(sameCount2 == 4) { fourOfAKind2 = cards2[i]; }
		if(fourOfAKind1 != -1 && fourOfAKind2 != -1) { break; }
	}


	if(fourOfAKind1 > fourOfAKind2) { 

		return true; 

	} else {

		return false;
	} 
}

//Return true if ph1 better hand than ph2
bool Player::compareStraightFlush(PokerHand &ph1, PokerHand &ph2) {

	return compareStraight(ph1, ph2); //Same rule for ties
}
