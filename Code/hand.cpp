#include "hand.hpp"

using namespace std;

//Class related methods + constructors
Hand::Hand() { } //Constructor does nothing - deck not initiated

Hand::~Hand() { } //Deconstructor

void Hand::addCard(Card &c) {
	
	cards.push_back(c);
}

vector<Card> Hand::getCards() { return cards; }

//Returns ace as 14 by default
vector<int> Hand::getCardValues() {

	vector<int> cardValues;
	for(unsigned int i=0; i<cards.size(); i++) {

		int val = static_cast<int>(cards[i].getValue());
		if(val == 1) { val = 14; } //Return ace as 14
		cardValues.push_back(val);
	}

	return cardValues;
}

//Counts number of occurrences of each suit (orderd by clubs, diamonds, hearts, spades)
vector<int> Hand::getSuitCounts() {

	vector<int> suitCounts(4, 0); //Initially the counts for each suits is zero
	for(unsigned int i=0; i<cards.size(); i++) {

		int suit = static_cast<int>(cards[i].getSuit()); //Cast suit to int, e.g. 1 = clubs, 2 = diamonds etc.
		suitCounts[suit-1]++;	
	}

	return suitCounts;
}

Card Hand::getCard(int index) { return cards[index]; }
void Hand::setCard(int index, Card &c) { cards[index] = c; }

int Hand::getNumCards() { return (int) cards.size(); }

void Hand::print() { 

	for(unsigned int i=0; i<cards.size(); i++) {
		
		cards[i].print();
	}
}


void Hand::putBackInDeck(Deck &c) {

	for(unsigned int i=0; i<cards.size(); i++) {

		c.addCardToBottom(cards[i]); //If hand is unchanged, this puts cards back in original order they were taken from the deck
	}	

	cards.clear(); //Explicitly clear the hand to avoid issues
}
