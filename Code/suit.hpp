//
//  suit.hpp
//  Poker_Project
//
//  Created by Casper Lumby on 16/11/2015.
//  Copyright © 2015 Casper Lumby. All rights reserved.
//

#ifndef suit_hpp
#define suit_hpp

enum struct Suit {
	
	CLUBS = 1,
	DIAMONDS = 2,
	HEARTS = 3,
	SPADES = 4,

};

#endif /* suit_hpp */
