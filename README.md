# README #


## Introduction ##

This repository contains C++ code for the public engagement activity described in "The fitness landscape of poker: a simple model for teaching evolutionary theory". The main program executes one million simulations of the evolution based poker game (see paper for full details) and outputs relevant statistics.

## Instructions ##

To run the program, follow this short guide on a Unix machine.

### Prerequisites ###

This C++ program requires use of functions defined in the GNU Scientific Library (GSL). Before attempting to download the program, please ensure there is a recent version of GSL installed. GSL may be downloaded from [https://www.gnu.org/software/gsl/](https://www.gnu.org/software/gsl/). Please note the installation paths.

### Download and compilation ###

The following steps describes the installation process:

* Download this project as a .zip file from this webpage
* Extract .zip file to directory of choice
* Navigate to the Code folder using a terminal
* Update the lines "CC_FLAGS" and "LD_FLAGS" in the Makefile with the correct paths for the GSL library (see Prerequisites). The paths are likely to be of the type "/usr/local/gsl/include" and "/usr/local/gsl/lib"
* With a compiler supporting C++11 run "make clean" followed by "make main" to compile the program

### Run ###

The simulation is executed by running "./main.run" from the command line. The relevant output is printed to the terminal following evaluation.

## Contact ##

Please address any questions to Chris Illingworth, [chris.illingworth@gen.cam.ac.uk](mailto:chris.illingworth@gen.cam.ac.uk).